package com.sampleasynctask;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.TextView;

import com.sampleasynctask.MyAsyncTask.onFinishedListener;

public class MainActivity extends ActionBarActivity {

	MyAsyncTask task;
	TextView tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		tv = (TextView) findViewById(R.id.textView1);
		
		task = new MyAsyncTask(new onFinishedListener() {
			
			@Override
			public void onFinished(String result) {
				Log.v("TEST AYSNC TASK", result);
				tv.setText(result);
				
			}
		});
		task.execute();
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (task!=null && task.getStatus()==Status.RUNNING)
			task.cancel(true);
		
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (task!=null && task.getStatus()==Status.RUNNING)
			task.cancel(true);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (task!=null && task.getStatus()!=Status.RUNNING)
			task.execute();
	}
	
}
