package com.sampleasynctask;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import android.text.TextUtils;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

/**
 * This class is a util for the HTTP client. 
 * It uses the OkHttp library by Square.
 * 
 * @author user
 */
public class OkHttpUtil {

	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_METHOD_POST = "POST";
	public static final String HTTP_METHOD_PUT = "PUT";

	final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	private String url;
	private Map<String, String> headers;
	private String body = "";
	private String httpMethod = "";
	private int responseCode;

	private OkHttpClient client;
	
	final static HostnameVerifier VERIFIER = new HostnameVerifier() {

		@Override
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	public OkHttpUtil(String httpMethod, String url, Map<String, String> headers, String body) {
		super();
		this.httpMethod = httpMethod;
		this.url = url;
		this.headers = headers;
		this.body = body;

		initClient();
	}

	public OkHttpUtil(String httpMethod, String url, Map<String, String> headers) {
		super();
		this.httpMethod = httpMethod;
		this.url = url;
		this.headers = headers;

		initClient();
	}

	public OkHttpUtil(String url, Map<String, String> headers, String post_body) {
		super();
		this.body = post_body;
		this.url = url;
		this.headers = headers;

		initClient();
	}

	public OkHttpUtil(String url, String post_body) {
		super();
		this.url = url;
		this.body = post_body;
		initClient();
	}

	private void initClient() {
		client = new OkHttpClient();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String execute() {
		Builder builder = new Builder();
		builder.url(url);

		if (!TextUtils.isEmpty(body)) {
			RequestBody reqBody = RequestBody.create(JSON, body);
			builder.post(reqBody);
		}

		if (headers != null) {
			Iterator it = headers.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
				builder.addHeader(pairs.getKey(), pairs.getValue());
				it.remove(); // avoids a ConcurrentModificationException
			}
		}

		Request request = builder.build();

		System.out.println("Request Headers: " + request.headers().toString());

		try {
			Response response = client.newCall(request).execute();
			String responseDump = response.body().string();
			String responseHeader = response.headers().toString();
			System.out.println("Response Headers: " + responseHeader);
			System.out.println(responseDump);

			setResponseCode(response.code());

			return responseDump;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

}
