package com.sampleasynctask;

import android.os.AsyncTask;

/**
 * This an aysnctask that will call a URL and has an
 * Interface to post the result in the main thread.
 * 
 * @author user
 */
public class MyAsyncTask extends AsyncTask<Void, Void, String>{

	private onFinishedListener listener;
	private OkHttpUtil httpClient;
	
	public MyAsyncTask(onFinishedListener listener) {
		super();
		this.listener = listener;
		String url ="http://api.citybik.es/networks.json";
		httpClient = new OkHttpUtil(OkHttpUtil.HTTP_METHOD_GET, url, null);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}
	
	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		if (!isCancelled())
			return httpClient.execute();
		else
			return "";
		
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		listener.onFinished(result);
	}

	public interface onFinishedListener{
		public void onFinished(String result);
	}

}